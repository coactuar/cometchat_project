-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 19, 2022 at 01:07 PM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cometchat`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `token`, `created_at`) VALUES
(82, 'neerajreddy', '$2y$10$XBvczvjoBbLy1CENCvNvNO4CYxQvYOl7GKJkUqQezYhjqs1dVuMDK', 'neerajreddy_16388062441d8a971d60ac4e450ef4e63760408d', '2021-12-06 15:57:09'),
(83, 'rakshith', '$2y$10$PQPcLPZOzAdIZuawJi0DxeKPxo5jhmzKgzRZDG5Ax5foAqZUE1AdG', 'rakshith_16388521356e4f918e8f15386c152ccff639aef6', '2021-12-07 04:42:04'),
(84, 'neeraj', '$2y$10$hY1U5/qYrp0VmEmXHVmEIeruF1Mr18BlAu6.IquL03NbbYYot4bWG', 'neeraj_1638860074f593d7b4685439202ce31ef0775357', '2021-12-07 06:54:23'),
(85, 'Likitha', '$2y$10$dhvefmWeUmM9wq7rFqln5eUaCqcLUXpp9FWGcuqp1pVfOV470MNxW', 'likitha_16388608947522866b8327df318b412685d200a2', '2021-12-07 07:07:56'),
(86, 'Anushree', '$2y$10$t4QxtFkrkIJmvREB2on/Ju/YPFAqb9Camj0xs7puBVjwvKw6iUZxq', 'anushree_163886094245ea5cb4c6ce1aef3fe5867b7d0be5', '2021-12-07 07:08:44'),
(87, 'priyanka', '$2y$10$EFuKM3ymlMsjTxpeL7wBIeMui5x.OOgJsEAm7cdZWCGJrgKznBRqe', 'priyanka_1638861002e5139cf3f12020b7043b49c1e09f8e', '2021-12-07 07:09:36'),
(88, 'priyanka123', '$2y$10$20/IOx6kqaz4DAKdYScorOZzsk8bCda2KOzQ5ccvuy.O0o4EIz0gG', 'priyanka123_16406036339dc8cc2fb4e4b51b651f3a46c0d4b8', '2021-12-27 11:13:40'),
(89, 'reddy', '$2y$10$PFzW2VtQiLJyPMSMxuyNCuKwV4oYl02fliVCY.kaH7gs6KDAqumje', 'reddy_164061272754caa4b56b7789fc42ed50ac7c00a9', '2021-12-27 13:45:14'),
(90, 'reddy9989', '$2y$10$q7cqvfUR4tcqEmz1/gCTOevQHbt1OmeXJWm.lpOsoIvHdK.2fNe52', 'reddy9989_1640672152b7d78d9e616b95b7e5c9cd1769fe70', '2021-12-28 06:06:40'),
(91, 'neerajreddy9989', '$2y$10$m4AttAsMzoJSQ4RCeovjsusKeoDPa36C5KJdW.YI4Bno/eq0/SBRa', 'neerajreddy9989_1640673596b182a7271714da680e3ee9bad1b52d', '2021-12-28 06:39:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
